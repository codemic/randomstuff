import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'rockpaperscissors',
    loadChildren: () => import('./rockpaperscissors/rockpaperscissors.module').then( m => m.RockpaperscissorsPageModule)
  },
  {
    path: 'randomnumber',
    loadChildren: () => import('./randomnumber/randomnumber.module').then( m => m.RandomnumberPageModule)
  },
  {
    path: 'randomword',
    loadChildren: () => import('./randomword/randomword.module').then( m => m.RandomwordPageModule)
  },
  {
    path: 'choicemaker',
    loadChildren: () => import('./choicemaker/choicemaker.module').then( m => m.ChoicemakerPageModule)
  },
  {
    path: 'dice',
    loadChildren: () => import('./dice/dice.module').then( m => m.DicePageModule)
  },
  {
    path: 'randomemoji',
    loadChildren: () => import('./randomemoji/randomemoji.module').then( m => m.RandomemojiPageModule)
  },
  {
    path: 'randomletter',
    loadChildren: () => import('./randomletter/randomletter.module').then( m => m.RandomletterPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
