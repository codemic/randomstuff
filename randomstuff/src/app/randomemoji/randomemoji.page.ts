import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-randomemoji',
  templateUrl: './randomemoji.page.html',
  styleUrls: ['./randomemoji.page.scss'],
})
export class RandomemojiPage implements OnInit {
  emoji = ['✌', '😂', '😝', '😁', '😱', '👉', '🙌', '🍻', '🔥', '🌈', '☀', '🎈', '🌹', '💄', '🎀', '⚽', '🎾', '🏁', '😡', '👿', '🐻', '🐶', '🐬', '🐟', '🍀', '👀', '🚗', '🍎', '💝', '💙', '👌', '❤', '😍', '😉', '😓', '😳', '💪', '💩', '🍸', '🔑', '💖', '🌟', '🎉', '🌺', '🎶', '👠', '🏈', '⚾', '🏆', '👽', '💀', '🐵', '🐮', '🐩', '🐎', '💣', '👃', '👂', '🍓', '💘', '💜', '👊', '💋', '😘', '😜', '😵', '🙏', '👋', '🚽', '💃', '💎', '🚀', '🌙', '🎁', '⛄', '🌊', '⛵', '🏀', '🎱', '💰', '👶', '👸', '🐰', '🐷', '🐍', '🐫', '🔫', '👄', '🚲', '🍉', '💛', '💚']
  selected = '✌';

  constructor() { }

  ngOnInit() {
    this.generate();
  }

  generate() {
    for (let i = 0; i < 10; i++) {
      setTimeout(() => {
        this.selected = this.emoji[Math.floor(Math.random() * this.emoji.length)];
      }, Math.floor(Math.random() * 520) + 400);
    }
  }
}
