import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RandomemojiPageRoutingModule } from './randomemoji-routing.module';

import { RandomemojiPage } from './randomemoji.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RandomemojiPageRoutingModule
  ],
  declarations: [RandomemojiPage]
})
export class RandomemojiPageModule {}
