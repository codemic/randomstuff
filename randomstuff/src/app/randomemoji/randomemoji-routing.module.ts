import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RandomemojiPage } from './randomemoji.page';

const routes: Routes = [
  {
    path: '',
    component: RandomemojiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RandomemojiPageRoutingModule {}
