import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-dice',
  templateUrl: './dice.page.html',
  styleUrls: ['./dice.page.scss'],
})
export class DicePage implements OnInit {
  @ViewChild('cube_one') cubeOne;
  @ViewChild('cube_two') cubeTwo;

  y = 0;
  isok = true;

  constructor() {

  }

  ngOnInit() {
    // this.dice();
  }

  afterViewInit() {
    this.dice();
  }

  rotate(d, k, el) {
    const numberOne = Math.floor(Math.random() * d) * k;
    const numberTwo = Math.floor(Math.random() * d) * k;
    el.nativeElement.style.transform = 'rotateY(' + numberOne + 'deg) rotateX(' + numberTwo + 'deg)';
  }

  dice() {
    const choose = setInterval(() => {

      this.rotate(3, 180, this.cubeOne);
      this.rotate(3, 180, this.cubeTwo);
      this.y += 1;

      if (this.y === 5) {
        this.rotate(5, 90, this.cubeOne);
        this.rotate(5, 90, this.cubeTwo);
        this.isok = true;
        clearInterval(choose);
        this.y = 0;
      }
    }, 400);
  }

}


















