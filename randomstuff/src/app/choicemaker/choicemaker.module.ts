import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChoicemakerPageRoutingModule } from './choicemaker-routing.module';

import { ChoicemakerPage } from './choicemaker.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChoicemakerPageRoutingModule
  ],
  declarations: [ChoicemakerPage]
})
export class ChoicemakerPageModule {}
