import { Component, OnInit, ViewChild } from '@angular/core';
import * as confetti from 'canvas-confetti';
@Component({
  selector: 'app-choicemaker',
  templateUrl: './choicemaker.page.html',
  styleUrls: ['./choicemaker.page.scss'],
})
export class ChoicemakerPage implements OnInit {
  @ViewChild('canvas', { static: true }) canvas;
  question;
  optionOne;
  optionTwo;
  optionThree;
  chosen;

  constructor() {
    this.question = '';
    this.optionOne = '';
    this.optionTwo = '';
    this.optionThree = '';
    this.chosen = null;

  }

  ngOnInit() {
  }

  choose() {
    for (let i = 0; i < 10; i++) {
      setTimeout(() => {
        let count = 1;
        if (this.optionTwo !== '') {
          count += 1;
        }
        if (this.optionThree !== '') {
          count += 1;
        }
        this.chosen = Math.floor(Math.random() * count) + 1;
      }, Math.floor(Math.random() * 520) + 400);
    }
    confetti.create(this.canvas.nativeElement, {
      resize: true,
      useWorker: true
    })({
      shapes: ['square'],
      particleCount: 500,
      spread: 1890,
      zIndex: 9999999,
      resize: true,
      origin: {
        x: 0.5,
        y: 1
      }
    });
    // confetti.create()({
    //   particleCount: 200,
    //   angle: 60,
    //   spread: 55,
    //   origin: { x: 0 },
    // });

  }


}

