import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChoicemakerPage } from './choicemaker.page';

const routes: Routes = [
  {
    path: '',
    component: ChoicemakerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChoicemakerPageRoutingModule {}
