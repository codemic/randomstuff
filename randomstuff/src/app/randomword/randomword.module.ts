import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RandomwordPageRoutingModule } from './randomword-routing.module';

import { RandomwordPage } from './randomword.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RandomwordPageRoutingModule
  ],
  declarations: [RandomwordPage]
})
export class RandomwordPageModule {}
