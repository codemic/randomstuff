import { Component, OnInit } from '@angular/core';
import { dictionary } from './dictionary';
@Component({
  selector: 'app-randomword',
  templateUrl: './randomword.page.html',
  styleUrls: ['./randomword.page.scss'],
})
export class RandomwordPage implements OnInit {

  word = '';
  definition = '';
  keys = [];
  constructor() {
    this.keys = Object.keys(dictionary);
    this.word = this.keys[Math.floor(Math.random() * this.keys.length)];
    this.definition = dictionary[this.word];
  }

  ngOnInit() {

  }

  generate() {
    this.word = this.keys[Math.floor(Math.random() * this.keys.length)];
    this.definition = dictionary[this.word];
  }


}
