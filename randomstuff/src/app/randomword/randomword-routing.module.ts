import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RandomwordPage } from './randomword.page';

const routes: Routes = [
  {
    path: '',
    component: RandomwordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RandomwordPageRoutingModule {}
