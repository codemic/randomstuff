import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RandomletterPageRoutingModule } from './randomletter-routing.module';

import { RandomletterPage } from './randomletter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RandomletterPageRoutingModule
  ],
  declarations: [RandomletterPage]
})
export class RandomletterPageModule {}
