import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-randomletter',
  templateUrl: './randomletter.page.html',
  styleUrls: ['./randomletter.page.scss'],
})
export class RandomletterPage implements OnInit {

  letters = [];
  letter = '';

  constructor() {
    this.letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
      'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  }

  ngOnInit() {
    this.generate();
  }

  generate() {
    for (let i = 0; i < 10; i++) {
      setTimeout(() => {
        const chosen = this.letters[Math.floor(Math.random() * this.letters.length)];
        this.letter = chosen;
      }, Math.floor(Math.random() * 520) + 200);
    }
  }

}
