import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RandomletterPage } from './randomletter.page';

const routes: Routes = [
  {
    path: '',
    component: RandomletterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RandomletterPageRoutingModule {}
