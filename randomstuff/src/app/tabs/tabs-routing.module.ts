import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'randomnumber',
        loadChildren: () => import('../randomnumber/randomnumber.module').then(m => m.RandomnumberPageModule)
      },
      {
        path: 'rockpaperscissors',
        loadChildren: () => import('../rockpaperscissors/rockpaperscissors.module').then(m => m.RockpaperscissorsPageModule)
      },
      {
        path: 'randomletter',
        loadChildren: () => import('../randomletter/randomletter.module').then(m => m.RandomletterPageModule)
      },
      {
        path: 'randomemoji',
        loadChildren: () => import('../randomemoji/randomemoji.module').then(m => m.RandomemojiPageModule)
      },
      {
        path: 'randomdice',
        loadChildren: () => import('../dice/dice.module').then(m => m.DicePageModule)
      },
      {
        path: 'randomword',
        loadChildren: () => import('../randomword/randomword.module').then(m => m.RandomwordPageModule)
      },
      {
        path: 'choicemaker',
        loadChildren: () => import('../choicemaker/choicemaker.module').then(m => m.ChoicemakerPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/randomnumber',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/randomnumber',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule { }
