import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RockpaperscissorsPageRoutingModule } from './rockpaperscissors-routing.module';

import { RockpaperscissorsPage } from './rockpaperscissors.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RockpaperscissorsPageRoutingModule
  ],
  declarations: [RockpaperscissorsPage]
})
export class RockpaperscissorsPageModule {}
