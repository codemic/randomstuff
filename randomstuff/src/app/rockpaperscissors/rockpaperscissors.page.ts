import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rockpaperscissors',
  templateUrl: './rockpaperscissors.page.html',
  styleUrls: ['./rockpaperscissors.page.scss'],
})
export class RockpaperscissorsPage implements OnInit {

  images = [];
  name = '';
  url = '';

  constructor() {
    this.images = [
      {
        name: 'Rock',
        svg: '../../assets/rock.svg'
      },
      {
        name: 'Paper',
        svg: '../../assets/paper.svg'
      },
      {
        name: 'Scissors',
        svg: '../../assets/scissors.svg'
      },

    ];
  }

  ngOnInit() {
    this.generate();
  }

  generate() {
    for (let i = 0; i < 10; i++) {
      setTimeout(() => {
        const chosen = this.images[Math.floor(Math.random() * this.images.length)];
        this.name = chosen.name;
        this.url = chosen.svg;
      }, Math.floor(Math.random() * 520) + 400);
    }
  }

}
