import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-randomnumber',
  templateUrl: './randomnumber.page.html',
  styleUrls: ['./randomnumber.page.scss'],
})
export class RandomnumberPage implements OnInit {

  randomNumber: number;
  min = 0;
  max = 10;
  error = '';
  animation = true;

  constructor() { }

  ngOnInit() {
    this.randomInteger();
  }

  randomInteger() {
    if (this.check()) {
      if (this.animation) {
        for (let i = 0; i < 10; i++) {
          setTimeout(() => {
            this.randomNumber = Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
          }, Math.floor(Math.random() * 250));
        }
      }
      this.randomNumber = Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
    }
  }

  check() {
    if (this.min > this.max) {
      this.error = 'Min value cannot be bigger than max';
      return false;
    } else if (this.max < this.min) {
      this.error = 'Max value cannot be bigger than min';
      return false;
    } else {
      this.error = '';
    }
    return true;
  }

}
