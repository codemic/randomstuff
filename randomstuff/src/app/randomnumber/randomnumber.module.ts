import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RandomnumberPageRoutingModule } from './randomnumber-routing.module';

import { RandomnumberPage } from './randomnumber.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RandomnumberPageRoutingModule
  ],
  declarations: [RandomnumberPage]
})
export class RandomnumberPageModule {}
